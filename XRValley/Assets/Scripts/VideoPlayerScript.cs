﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;//you need to call the video namespace, it won't automatically be there.

public class VideoPlayerScript : MonoBehaviour {

    public Material playbutton;
    public Material pausebutton;
    public Renderer playButtonrend; 

    private VideoPlayer sixrVideo; 
    

    void Awake()
    {
        sixrVideo = GetComponent<VideoPlayer>();
    }
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	        if (Input.GetMouseButtonDown(0) && sixrVideo.isPlaying)
        {
            sixrVideo.Pause();
            playButtonrend.material = playbutton;
        }
        else if (Input.GetMouseButtonDown(0))
        {
            sixrVideo.Play();
            playButtonrend.material = pausebutton;
        }
    }
}
