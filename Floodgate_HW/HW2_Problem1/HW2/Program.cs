﻿using System;

namespace HW2
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            // print input prompt
            Console.WriteLine("Input the 1st number:");
            // take and store answer
            int ans1 = Convert.ToInt32(Console.ReadLine());

            //ask for second number
            Console.WriteLine("Input the 2nd number:");
            //store second value
            int ans2 = Convert.ToInt32(Console.ReadLine());

            //ask for third number
            Console.WriteLine("Input the 3rd fnumber");
            //store third value
            int ans3 = Convert.ToInt32(Console.ReadLine());


            // prepare message
            String mInit = "The ";
            String mMid = "";           //this message will store placement of the highest number
            String mEnding = " Number is the greatest among three";


            // if statements to compare values
            if (ans1 > ans2 && ans1 > ans3){
                // first number is the greatest
                mMid = "1st";
            }
            else if (ans2 > ans1 && ans2 > ans3){
                // second number is the greatest
                mMid = "2nd";
            }
            else if ( ans3 > ans1 && ans3 > ans2){
                // third number is the greatest
                mMid = "3rd";
            }
            Console.WriteLine(mInit + mMid + mEnding);
        }
    }
}
