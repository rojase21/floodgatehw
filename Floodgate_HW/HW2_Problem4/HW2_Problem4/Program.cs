﻿using System;

namespace HW2_Problem4
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            // create for loop to count 1-100
            for (int i = 1; i <= 100; i++){

                //create bool to control print of numbers
                bool printWord = false;

                //print "Fizz" if multiple of 3
                if ((i%3) ==0){
                    Console.Write("Fizz");
                    printWord = true;
                }

                //print "Buzz" if multiple of 5
                if ((i%5)==0){
                    Console.Write("Buzz");
                    printWord = true;
                }

                //if a word hasn't been printed, then print a number
                if (!printWord){
                    Console.Write(i);
                }

                // insert space between characters
                Console.Write(" ");
            }
        }
    }
}
