﻿using System;

public class Program
{
    public static void Main()
    {
        Console.WriteLine("\t\tMenu\n");

        Console.WriteLine("Press 1 for add\n");

        Console.WriteLine("Press 2 for subtraction\n");

        Console.WriteLine("Press 3 for multiplication\n");

        Console.WriteLine("Press 4 for division\n\n");

        Console.WriteLine("Enter first number : \t");
        int num1 = Convert.ToInt32(Console.ReadLine());

        Console.WriteLine("Enter second number : \t");
        int num2 = Convert.ToInt32(Console.ReadLine());

        Console.WriteLine("\n");


        Console.WriteLine("Enter your option");
        int ans = Convert.ToInt32(Console.ReadLine());

        String op = "";
        float dispAns = 0;

        if (ans == 1)
        {
            //add
            dispAns = num1 + num2;
            op = " + ";
        }
        else if (ans == 2)
        {
            dispAns = num1 - num2;
            op = " - ";
        }
        else if (ans == 3)
        {
            dispAns = num1 * num2;
            op = " * ";
        }
        else if (ans == 4)
        {
            dispAns = (float)num1 / num2;
            op = " / ";
        }
        Console.WriteLine(num1 + op + num2 + " = " + dispAns);


    }
}