﻿using System;

namespace ArraysNMethods
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("=================================\n");
            Console.WriteLine("Solution 1\n");

            // instantiate solution 1 class
            Solution1 prob1 = new Solution1();

            // run main program solution
            prob1.main();

            Console.WriteLine("\n=================================");


            Console.WriteLine("=================================\n");
            Console.WriteLine("Solution 2\n");

            // instantiate solution class
            Solution2 prob2 = new Solution2();

            // run main program solution
            prob2.main();

            Console.WriteLine("\n=================================");


            Console.WriteLine("=================================\n");
            Console.WriteLine("Solution 3\n");

            // instantiate solution class
            Solution3 prob3 = new Solution3();

            // run main program solution
            prob3.main();

            Console.WriteLine("\n=================================");

            Console.WriteLine("=================================");
            Console.WriteLine("Solution 4");

            // instantiate solution class
            Solution4 prob4 = new Solution4();

            // run main program solution
            prob4.main();

            Console.WriteLine("=================================");


            Console.WriteLine("=================================");
            Console.WriteLine("Solution 5");

            // instantiate solution class
            Solution5 prob5 = new Solution5();

            // run main program solution
            prob5.main();

            Console.WriteLine("=================================");


            Console.WriteLine("=================================");
            Console.WriteLine("Solution 6");

            // instantiate solution class
            Solution6 prob6 = new Solution6();

            // run main program solution
            prob6.main();

            Console.WriteLine("=================================");


            Console.WriteLine("=================================");
            Console.WriteLine("Solution 7");

            // instantiate solution class
            Solution7 prob7 = new Solution7();

            // run main program solution
            prob7.main();

            Console.WriteLine("=================================");

        }
    }
}
