﻿using System;
namespace ArraysNMethods
{
    public class Solution6
    {
        public void main()
        {
            // specify array to store list
            int[] elements = new int[10];


            // prompt user for 10 elements
            Console.WriteLine("Input 10 elements in the array:");

            for (int i = 0; i < 10; i++)
            {
                // write element number
                Console.Write("element - {0} : ", i);
                elements[i] = Convert.ToInt32(Console.ReadLine());

            }

            // print output message
            Console.Write("The even numbers are ");

            // iterate throught the array and print the even numbers
            foreach (int val in elements)
            {

                // conditional operation to check if even
                if (val % 2 == 0)
                {
                    Console.Write(val + " ");
                }
            }
        }
    }
}
