﻿using System;
namespace ArraysNMethods
{
    public class Solution1
    {
        public void main()
        {
            // specify array to store list
            int[] elements = new int[10];


            // prompt user for 10 elements
            Console.WriteLine("Input 10 elements in the array:");

            for (int i = 0; i < 10; i++)
            {
                // write element number
                Console.Write("element - {0} : ", i);
                elements[i] = Convert.ToInt32(Console.ReadLine());

            }

            // Printed the Output
            Console.Write("Elements in array are: ");
            for (int i = 0; i < elements.Length; i++)
            {
                Console.Write(elements[i] + " ");
            }
        }


    }
}
