﻿using System;
namespace ArraysNMethods
{
    public class Solution3
    {
        public void main()
        {
            // Prompt user for size of array & initialize array
            Console.Write("Input the number of elements to store in the array:");
            // reads user input, converts it to int
            //  and uses it to define lenght of new array
            int[] elements = new int[Convert.ToInt32(Console.ReadLine())];


            // prompt user for elements
            Console.WriteLine("Input {0} elements in the array:", elements.Length);

            for (int i = 0; i < elements.Length; i++)
            {
                // write element number
                Console.Write("element - {0} : ", i);
                elements[i] = Convert.ToInt32(Console.ReadLine());
            }

            // create value to store sum
            int sum = 0;

            // iterate thorugh array and add values
            foreach (int val in elements)
            {
                sum += val;
            }

            // print sum of array values
            Console.WriteLine("Sum of all elements stored in the array is: {0}", sum);

        }
    }
}
