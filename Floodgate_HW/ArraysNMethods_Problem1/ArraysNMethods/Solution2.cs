﻿using System;
namespace ArraysNMethods
{
    public class Solution2
    {
        public void main()
        {
            // Prompt user for size of array & initialize array
            Console.Write("Input the number of elements to store in the array:");
            // reads user input, converts it to int
            //  and uses it to define lenght of new array
            int[] elements = new int[Convert.ToInt32(Console.ReadLine())];


            // prompt user for 10 elements
            Console.WriteLine("Input {0} elements in the array:", elements.Length);

            for (int i = 0; i < elements.Length; i++)
            {
                // write element number
                Console.Write("element - {0} : ", i);
                elements[i] = Convert.ToInt32(Console.ReadLine());

            }

            // Printed the Output
            Console.WriteLine("The values stored in the array are:");
            for (int i = 0; i < elements.Length; i++)
            {
                Console.Write(elements[i] + " ");
            }
            Console.WriteLine("");

            // Printed the Output in reverse
            Console.WriteLine("The values stored in the array in reverse are:");
            for (int i = elements.Length; i > 0; i--)
            {
                Console.Write(elements[i - 1] + " ");
            }

        }
    }
}
