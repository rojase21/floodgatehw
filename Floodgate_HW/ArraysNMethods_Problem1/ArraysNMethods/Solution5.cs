﻿using System;
namespace ArraysNMethods
{
    public class Solution5
    {
        public void main()
        {
            //Prompt user for string
            Console.WriteLine("Input the string to be reversed: ");

            // Store value a a string
            String str = Console.ReadLine();

            // Convert string to array of Chars
            char[] ch = str.ToCharArray();

            Console.Write("Reversed string is: ");

            // iterate through array in reverse and print charaters
            for (int i = ch.Length - 1; i >= 0; i--)
            {
                Console.Write(ch[i]);
            }
        }
    }
}
