﻿using System;

namespace HW2_Problem2
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            // prompt user for test data
            Console.Write("Test Data: ");

            //read and store test data
            int ans = Convert.ToInt32(Console.ReadLine());

            // determine if ans is odd or even
            if ((ans%2) == 0)
            {
                //even number 
                Console.WriteLine(ans + " is an even integer");
            }
            else {
                //odd number
                Console.WriteLine(ans + " is an odd integer");
            }
        }
    }
}
