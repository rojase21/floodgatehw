﻿using System;

namespace HW2_Problem3
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            // prompt user for test data
            Console.Write("Test Data: ");
            //read and store test data
            //convert to character 
            Char c = Convert.ToChar(Console.ReadLine());

            // compare character(response) to list of vowels
            // vowels = aeiouAEIOU
            // bool returns true if it is a vowel
            bool isVowel = "aeiouAEIOU".IndexOf(c) >= 0;

            // check type of character
            if (isVowel){
                // answer is a vowel
                Console.WriteLine("The alphabet is a vowel");
            } else {
                // answer is a consonant
                Console.WriteLine("The alphabet is a consonant");
            }

        }
    }
}
