﻿using System;

namespace wk2_Class1
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            Console.Write("Input number of terms: ");
            int num = Convert.ToInt32(Console.ReadLine());


            Console.WriteLine("Using For Loop");

            for (int i = 1; i <= num; i = i +4){
                Console.Write("Number is " + i);
                int cube = i * i * i;
                Console.WriteLine(" and cube of the " + i + " is :" + cube);
            }

            Console.WriteLine("Using do, While");

            int j = 1;
            do
            {
                Console.Write("Number is " + j);
                int cube = j * j * j;
                Console.WriteLine(" and cube of the " + j + " is :" + cube);
                j++;
            } while (j <= num);

            Console.WriteLine("Using While Loop");


            int k = 1;
            while(k<=num){
                Console.Write("Number is " + k);
                int cube = k * k * k;
                Console.WriteLine(" and cube of the " + k + " is :" + cube);
                k++;
            }
        }
    }
}
