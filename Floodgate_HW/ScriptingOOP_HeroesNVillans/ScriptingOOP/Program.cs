﻿using System;


namespace ScriptingOOP
{
    class MainClass
    {
        public static void Main(string[] args)
        {
            //Accept user input for Hero's Name & Weapons
            Console.WriteLine("\n======Enter Hero's info=====");

            //Get Hero's Name
            string hName = GetName();

            // Accept Hero's Weapons
            string[] hWeapons = GetWeapons();

            // Create new hero with collected info
            Hero hero = new Hero(hName, hWeapons);


            //Accept user input for villian's name & Weapons
            Console.WriteLine("\n======Enter Villian's info=====");

            //Get Hero's Name
            string vName = GetName();

            // Accept Hero's Weapons
            string[] vWeapons = GetWeapons();

            // Create new hero with collected info
            Villain villain = new Villain(vName, vWeapons);

            // print introductions
            Console.WriteLine("\n======Introductions=====");

            Console.Write("Hero:");
            //Print Hero's Name (SayHello method)
            hero.SayHello();

            //Print Hero's Weapons (ShowWeapons Method)
            hero.ShowWeapons();

            Console.Write("\nVillian:");

            //Print Villain's Name (SayHello method)
            villain.SayHello();

            //Print Villain's Weapons (ShowWeapons Method)
            villain.ShowWeapons();


            //Fight!

            Console.WriteLine("\n======READY. FIGHT!!!=====");

            // Call Hero's attack method (Attack Method)
            villain.Life = hero.Attack(villain.Life);
            //Console.WriteLine("Hero's life is {0}", hero.Life);

            // Call Villain's attack method (Attack Method)
            hero.Life = villain.Attack(hero.Life);
            //Console.WriteLine("Villain's life is {0}", villain.Life);

            // Output the winner after the attack. (whoever has highest life)
            FindWinner(hero.Life, villain.Life);
        }

        //function that find the winner and prints out their name
        public static void FindWinner(int hero, int villain)
        {
            if (hero > villain)
            {
                Console.WriteLine("The Hero wins!");
            } else if (villain > hero)
            {
                Console.WriteLine("The Villian wins!!");
            }
            else
            {
                Console.WriteLine("Players have tied!!!");
            }
        }


        //prompts user for name and receives a string back
        public static string GetName()
        {
            Console.Write("Name: ");
            string charName = Console.ReadLine();
            return charName;
        }

        //prompts user for number of weapons and returns a list fo strings
        public static string[] GetWeapons()
        {
            Console.Write("How many Weapons ");
            int numWeapons = Convert.ToInt32(Console.ReadLine());

            string[] charWeapons = new string[numWeapons];

            for (int i = 0; i < numWeapons; i++)
            {
                Console.Write("Weapon number {0}:", i + 1);
                charWeapons[i] = Console.ReadLine();
            }
            return charWeapons;
        }
    }


    // Character class (base class)
    class Character
    {
        // create attributesa
        private string name;
        private string[] weapons;
        private int life;

        public Character()
        {
            //Console.WriteLine("zero Constructor");
            //Name = "";
            //Weapons = new string[0];
            //Life = 0;
        }


        // constructor that gives character name, weapons, lif
        public Character(string name, string[] weapons, int life)
        {

            //name Attribute
            Name = name;

            //Weapons Attribute
            Weapons = weapons;

            //Life Attribute
            Life = life;
        }

        //SayHello method
        //Output prints their name
        public void SayHello()
        {
            Console.WriteLine("Hello my name is {0} ", Name);
        }

        //Show Weapons method
        //Output Lists all the weapons they have in their arsenal
        public void ShowWeapons()
        {
            Console.Write("My {0} weapons are ", Weapons.Length);

            //iterate through array and print weapons
            for (int i = 0; i < Weapons.Length; i++)
            {
                Console.Write(Weapons[i] + " ");
            }
            Console.WriteLine();
        }

        //create getters and setters to acces private attributes of Character
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public string[] Weapons
        {
            get { return weapons; }
            set { weapons = value; }
        }

        public int Life
        {
            get { return life; }
            set { life = value; }
        }
    }

    //inherits from Character class
    class Hero : Character
    {

        //constructor that takes in name and array of weapons
        //automatically sets life to 100
        public Hero(string name, string[] weapons)
        {
            //redefine Character attributes to parameter input
            this.Name = name;
            this.Weapons = weapons;
            this.Life = 100;
        }

        //attack method
        //param1: Villians life
        //output: "Your attack of 87 left the villian with 63 life"
        public int Attack(int vLife)
        {
            // generate random attack number (0-80)
            Random rnd = new Random();
            int randAttack = rnd.Next(0, 80);

            // grab villian's life
            vLife = vLife - randAttack;
            
            Console.WriteLine("Hero's attack of {0} left the villian with {1} life", randAttack, vLife);
            return vLife;
        }

    }

    class Villain : Character
    {

        //constructor that takes in name and array of weapons
        //automatically sets life to 150
        public Villain(string name, string[] weapons)
        {
            //redefine Character attributes to parameter input
            this.Name = name;
            this.Weapons = weapons;
            this.Life = 150;
        }


        //attack method
        //param1: Villians life
        //output: "Your attack of 87 left the villian with 63 life"
        public int Attack(int hLife)
        {
            // generate random attack number (0-80)
            Random rnd = new Random();
            int randAttack = rnd.Next(0,50);

            // grab villian's life
            hLife = hLife - randAttack;

            Console.WriteLine("Villain's attack of {0} left the hero with {1} life", randAttack, hLife);
            return hLife;
        }
    }
}
